package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;

import br.com.condoservices.dao.PessoasDAO;

import br.com.condoservices.entidade.Pessoas;
import br.com.condoservices.entidade.PessoasVO;


@Secured
@Path("/pessoas")
public class PessoasService {
	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private PessoasDAO pessoasDAO;

	@PostConstruct
	private void init() {
		pessoasDAO = new PessoasDAO();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addPessoas(Pessoas pessoas) {
		JSONObject obj = new JSONObject();
		try {
			pessoasDAO.addPessoas(pessoas);
			obj.put("status", true);
			obj.put("message", "Residente adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			System.out.println(e);
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}
	
	@GET
	@Path("/list-condomino")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getListTipo() {
		List<PessoasVO> lista = new ArrayList<>();

		JSONObject obj = new JSONObject();

		try {
			lista = pessoasDAO.getListPessoas();
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
