package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.AreaComumDAO;
import br.com.condoservices.entidade.AreaComum;
import br.com.condoservices.entidade.AreaComumVO;


@Secured
@Path("/area-comum")
public class AreaComumService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private AreaComumDAO areaDAO;

	@PostConstruct
	private void init() {
		areaDAO = new AreaComumDAO();
	}

	@POST
	@Path("/add-area")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addUser(AreaComum area) {
		JSONObject obj = new JSONObject();

		try {
			areaDAO.addArea(area);
			obj.put("status", true);
			obj.put("message", "Area comum adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list-area/{cnpj}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getAreaComum(@PathParam("cnpj") String cnpj) {
		List<AreaComumVO> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = areaDAO.getAreas(cnpj);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@DELETE
	@Path("/delete-area/{id}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response deleteOcorrencia(@PathParam("id") int id) {

		JSONObject obj = new JSONObject();

		try {
			areaDAO.deleteAreaComum(id);

			obj.put("status", true);
			obj.put("message", "Area comum removido com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@DELETE
	@Path("/delete-reserva/{id}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response deleteReserva(@PathParam("id") int id) {

		JSONObject obj = new JSONObject();

		try {
			areaDAO.deleteReserva(id);

			obj.put("status", true);
			obj.put("message", "Reserva removido com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
