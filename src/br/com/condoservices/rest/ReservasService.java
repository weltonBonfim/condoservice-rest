package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.ReservasDAO;
import br.com.condoservices.entidade.Reservas;
import br.com.condoservices.entidade.ReservasVO;

@Secured
@Path("/reservas")
public class ReservasService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private ReservasDAO reservasDAO;

	@PostConstruct
	private void init() {
		reservasDAO = new ReservasDAO();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addReserva(Reservas reservas) {
		JSONObject obj = new JSONObject();

		try {
			boolean reserva = reservasDAO.addReserva(reservas);

			if (reserva) {
				obj.put("status", true);
				obj.put("message", "Reserva adicionado com sucesso!");
			} else {
				obj.put("status", false);
				obj.put("message", "Local e data ja reservados!");
			}

			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list/{cnpj}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getReservas(@PathParam("cnpj") String cnpj) {
		List<ReservasVO> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = reservasDAO.getReservas(cnpj);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
