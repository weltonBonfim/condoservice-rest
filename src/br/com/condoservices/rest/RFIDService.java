package br.com.condoservices.rest;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.RfidDAO;
import br.com.condoservices.entidade.Rfid;
import java.util.List;

@Secured
@Path("/rfid")
public class RFIDService {
	
	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private RfidDAO rfidDAO;

	@PostConstruct
	private void init() {
		rfidDAO = new RfidDAO();
	}
	
	@GET
	@Path("/validar-entrada/{rfid}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response verificarEntrada(@PathParam("rfid") String rfid) {
		
		JSONObject obj = new JSONObject();
		
		try {
			
			String retorno = rfidDAO.validEntrada(rfid);
						
			return Response.status(200).entity(retorno).build();
			
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}
	
	@PUT
	@Path("/ativa-rfid/{id}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response updatePrimeiroAcesso(@PathParam("id") String id ) {
		JSONObject obj = new JSONObject();

		try {
			rfidDAO.updateStatusRFID(id);
			obj.put("status", true);
			obj.put("message", "RFID ativado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();
		}
	}
	
	@GET
	@Path("/lista")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response listaRFID() {
		
		List<Rfid> lista = new ArrayList<>();
		
		JSONObject obj = new JSONObject();
		
		try {
			
			lista = rfidDAO.listaRFID();
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
			
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
