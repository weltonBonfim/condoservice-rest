package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.OcorrenciasDAO;
import br.com.condoservices.entidade.Ocorrencias;
import br.com.condoservices.entidade.OcorrenciasVO;

@Secured
@Path("/ocorrencias")
public class OcorrenciasService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private OcorrenciasDAO ocorrenciasDAO;

	@PostConstruct
	private void init() {
		ocorrenciasDAO = new OcorrenciasDAO();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addOcorrencia(Ocorrencias ocorrencia) {
		JSONObject obj = new JSONObject();

		try {
			ocorrenciasDAO.addOcorrencia(ocorrencia);
			obj.put("status", true);
			obj.put("message", "Ocorrencia adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list/{cnpj}/{tipo}/{login}/{status}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getOcorrencia(@PathParam("cnpj") String cnpj, @PathParam("tipo") String tipo,
			@PathParam("login") String login, @PathParam("status") String status) {
		List<OcorrenciasVO> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = ocorrenciasDAO.getOcorrencia(cnpj, tipo, login, status);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@PUT
	@Path("/encerrar-ocorrencia/{id}/{message}/{status}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response encerrarOcorrencia(@PathParam("id") int id, @PathParam("message") String message,
			@PathParam("status") String status) {

		JSONObject obj = new JSONObject();

		try {
			ocorrenciasDAO.encerrarOcorrencia(id, message, status);
			
			obj.put("status", true);
			obj.put("message", "Ocorrencia encerrada com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@PUT
	@Path("/reabrir-ocorrencia/{id}/{status}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response encerrarOcorrencia(@PathParam("id") int id, @PathParam("status") String status) {

		JSONObject obj = new JSONObject();

		try {
			ocorrenciasDAO.reabrirOcorrencia(id, status);

			obj.put("status", true);
			obj.put("message", "Ocorrencia reaberta com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}
}
