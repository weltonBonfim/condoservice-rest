package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.ComunicadosDAO;
import br.com.condoservices.entidade.Comunicados;
import br.com.condoservices.entidade.ComunicadosVO;

@Secured
@Path("/comunicados")
public class ComunicadosService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private ComunicadosDAO comunicadoDAO;

	@PostConstruct
	private void init() {
		comunicadoDAO = new ComunicadosDAO();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addOcorrencia(Comunicados ocorrencia) {
		JSONObject obj = new JSONObject();

		try {
			comunicadoDAO.addComunicado(ocorrencia);
			obj.put("status", true);
			obj.put("message", "Comunicado adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list/{cnpj}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getOcorrencia(@PathParam("cnpj") String cnpj) {
		List<ComunicadosVO> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = comunicadoDAO.getComunicados(cnpj);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response deleteOcorrencia(@PathParam("id") int id) {

		JSONObject obj = new JSONObject();

		try {
			comunicadoDAO.deleteComunicado(id);

			obj.put("status", true);
			obj.put("message", "Comunicado removido com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
