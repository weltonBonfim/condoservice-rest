package br.com.condoservices.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.CondominioDAO;
import br.com.condoservices.entidade.Condominio;

@Secured
@Path("/condominio")
public class CondominioService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private CondominioDAO condominioDAO;

	@PostConstruct
	private void init() {
		condominioDAO = new CondominioDAO();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarNotas() {

		List<Condominio> lista = null;

		try {
			lista = condominioDAO.listarCondominio();
			Gson gsonBuilder = new GsonBuilder().create();
			return Response.status(200).entity(gsonBuilder.toJson(lista)).build();
		} catch (Exception e) {

			return Response.status(400).entity("Erro ao recuperar dados do condominio" + e.getMessage()).build();
		}

	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addCondominio(Condominio condominio) {
		JSONObject obj = new JSONObject();

		try {
			condominioDAO.addCondominio(condominio);
			obj.put("status", true);
			obj.put("message", "Condominio adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response removerNota(@PathParam("id") int idCondominio) {
		JSONObject obj = new JSONObject();
		System.out.println(idCondominio);
		try {
			condominioDAO.removerCondominio(idCondominio);
			obj.put("status", true);
			obj.put("message", "Condominio removido com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();
		}
	}

}
