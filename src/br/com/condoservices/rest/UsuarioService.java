package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.UsuarioDAO;

import br.com.condoservices.entidade.Login;
import br.com.condoservices.entidade.TipoUsuario;
import br.com.condoservices.entidade.Usuarios;

@Secured
@Path("/user")
public class UsuarioService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private UsuarioDAO userDAO;

	@PostConstruct
	private void init() {
		userDAO = new UsuarioDAO();
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response login(String login) {

		JSONObject obj = new JSONObject(login);

		JSONObject objResponse = new JSONObject();

		List<Login> lista = new ArrayList<>();

		try {

			lista = userDAO.getLogin(obj.get("login").toString(), obj.get("senha").toString());

			if (!lista.isEmpty()) {
				objResponse.put("status", true);
				objResponse.put("message", "Usu�rio autenticado com sucesso!");
				objResponse.put("data", lista);
				return Response.status(200).entity(objResponse.toString()).build();
			} else {
				objResponse.put("status", false);
				objResponse.put("message", "Usuario ou senha invalidos!");
				return Response.status(200).entity(objResponse.toString()).build();
			}

		} catch (Exception e) {
			objResponse.put("error", "Erro ao efetuar login");
			objResponse.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();
		}

	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addUser(Usuarios usuario) {
		JSONObject obj = new JSONObject();

		try {
			userDAO.addUser(usuario);
			obj.put("status", true);
			obj.put("message", "Usu�rio adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list-tipo")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getListTipo() {
		List<TipoUsuario> lista = new ArrayList<>();

		JSONObject obj = new JSONObject();

		try {
			lista = userDAO.getListTipoUsuario();
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@PUT
	@Path("/primeiro-acesso/{login}/{senha}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response updatePrimeiroAcesso(@PathParam("login") String login, @PathParam("senha") String senha ) {
		JSONObject obj = new JSONObject();

		try {
			userDAO.updatePrimeiroAcesso(login, senha);
			obj.put("status", true);
			obj.put("message", "Senha alterada com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();
		}
	}

}
