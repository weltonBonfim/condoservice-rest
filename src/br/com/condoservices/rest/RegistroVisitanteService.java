package br.com.condoservices.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import br.com.condoservices.annotations.Secured;

import br.com.condoservices.dao.RegistroVisitanteDAO;

import br.com.condoservices.entidade.RegistroVisitante;
import br.com.condoservices.entidade.RegistroVisitanteVO;
import br.com.condoservices.entidade.VisitantesRegistros;

@Secured
@Path("/visitantes")
public class RegistroVisitanteService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private RegistroVisitanteDAO registroVisitanteDAO;

	@PostConstruct
	private void init() {
		registroVisitanteDAO = new RegistroVisitanteDAO();
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addOcorrencia(RegistroVisitante visitantes) {
		System.out.println("passou aqui");
		JSONObject obj = new JSONObject();
		
		try {
			registroVisitanteDAO.addVisitante(visitantes);
			obj.put("status", true);
			obj.put("message", "Visitante adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@GET
	@Path("/list/{cnpj}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getOcorrencia(@PathParam("cnpj") String cnpj) {
		List<RegistroVisitanteVO> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = registroVisitanteDAO.getVisitantes(cnpj);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

	@PUT
	@Path("/registrar-saida/{id}/{data-saida}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response encerrarOcorrencia(@PathParam("id") int id, @PathParam("data-saida") String data_saida) {

		JSONObject obj = new JSONObject();

		try {
			registroVisitanteDAO.registraSaida(id, data_saida);

			obj.put("status", true);
			obj.put("message", "Registro de sa�da realizado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();

		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}
	
	@GET
	@Path("/registros/{cnpj}/{data}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response getRegistros(@PathParam("cnpj") String cnpj, @PathParam("data") String data) {
		List<VisitantesRegistros> lista = new ArrayList<>();
		JSONObject obj = new JSONObject();

		try {
			lista = registroVisitanteDAO.getRegistros(cnpj, data);
			obj.put("lista", lista);
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}

}
