package br.com.condoservices.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.condoservices.annotations.Secured;
import br.com.condoservices.dao.AptoDAO;
import br.com.condoservices.entidade.Apartamentos;
import br.com.condoservices.entidade.ApartamentosVO;

@Secured
@Path("/apto")
public class ApartamentoService {
	private static final String CHARSET_UTF8 = ";charset=utf-8";
	private AptoDAO aptoDao;
	
	@PostConstruct
	private void init() {
		aptoDao = new AptoDAO();
	}
	
	@GET
	@Path("/list/{cnpj_condominio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarApto(@PathParam("cnpj_condominio") String cnpj_condominio) {

		List<ApartamentosVO> lista = null;

		try {
			lista = aptoDao.listarApto(cnpj_condominio);
			Gson gsonBuilder = new GsonBuilder().create();
			return Response.status(200).entity(gsonBuilder.toJson(lista)).build();
		} catch (Exception e) {

			return Response.status(400).entity("Erro ao recuperar dados dos apartamentos " + e.getMessage()).build();
		}

	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	public Response addApto(Apartamentos apto) {
		JSONObject obj = new JSONObject();
		try {
			aptoDao.addApto(apto);
			obj.put("status", true);
			obj.put("message", "Apartamento adicionado com sucesso!");
			return Response.status(200).entity(obj.toString()).build();
		} catch (Exception e) {
			System.out.println(e);
			obj.put("status", false);
			obj.put("message", e.getMessage());
			return Response.status(400).entity(obj.toString()).build();

		}
	}
}
