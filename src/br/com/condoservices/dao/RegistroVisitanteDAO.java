package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.RegistroVisitante;
import br.com.condoservices.entidade.RegistroVisitanteVO;
import br.com.condoservices.entidade.VisitantesRegistros;

public class RegistroVisitanteDAO {

	public void addVisitante(RegistroVisitante visitante) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_registro_visitante (nome_visitante, motivo_visita, data_entrada, id_apto, cnpj_condominio, doc_visitante, data_saida) VALUES(?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, visitante.getNome_visitante());
		statement.setString(2, visitante.getMotivo_visita());
		statement.setDate(3, java.sql.Date.valueOf(visitante.getdata_entrada()));
		statement.setInt(4, visitante.getId_apto());
		statement.setString(5, visitante.getCnpj_condominio());
		statement.setString(6, visitante.getDoc_visitante());
		statement.setDate(7, null);

		statement.execute();

	}

	public List<RegistroVisitanteVO> getVisitantes(String cnpj) throws Exception {
		List<RegistroVisitanteVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_registro_visitante WHERE cnpj_condominio=?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cnpj);
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			RegistroVisitanteVO visitante = new RegistroVisitanteVO();
			visitante.setId_visitante(rs.getInt("id_visitante"));
			visitante.setNome_visitante(rs.getString("nome_visitante"));
			visitante.setMotivo_visita(rs.getString("motivo_visita"));
			visitante.setData_entrada(rs.getString("data_entrada"));
			visitante.setId_apto(rs.getInt("id_apto"));
			visitante.setCnpj_condominio(rs.getString("cnpj_condominio"));
			visitante.setDoc_visitante(rs.getString("doc_visitante"));
			visitante.setData_saida(rs.getString("data_saida"));
			lista.add(visitante);
		}

		return lista;

	}

	public void registraSaida(int id, String data_saida) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "UPDATE tb_registro_visitante SET data_saida=? WHERE id_visitante = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setDate(1, java.sql.Date.valueOf(data_saida));
		statement.setInt(2, id);
		statement.execute();

	}

	public List<VisitantesRegistros> getRegistros(String cnpj, String data) throws Exception {

		List<VisitantesRegistros> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT (SELECT COUNT(data_entrada) AS entrada FROM db_condoservices.tb_registro_visitante WHERE data_entrada=? AND cnpj_condominio=?) AS 'entrada' \r\n"
				+ "UNION ALL\r\n"
				+ "SELECT (SELECT COUNT(data_saida) AS saida FROM db_condoservices.tb_registro_visitante WHERE data_saida=? AND cnpj_condominio=?) AS 'entrada';\r\n"
				+ "";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setDate(1, java.sql.Date.valueOf(data));
		statement.setString(2, cnpj);
		statement.setDate(3, java.sql.Date.valueOf(data));
		statement.setString(4, cnpj);
		ResultSet rs = statement.executeQuery();
		int count = 0;
		while (rs.next()) {
			VisitantesRegistros registros = new VisitantesRegistros();
			if (count == 0) {
				registros.setEntrada(rs.getString("entrada"));
				count++;
			} else {
				registros.setSaida(rs.getString("entrada"));
			}

			lista.add(registros);
		}

		return lista;
	}

}
