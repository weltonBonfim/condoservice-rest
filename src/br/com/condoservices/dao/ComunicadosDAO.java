package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.Comunicados;
import br.com.condoservices.entidade.ComunicadosVO;

public class ComunicadosDAO {

	public void addComunicado(Comunicados ocorrencia) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_comunicados (titulo, descricao, data_ocorrencia, nome_usuario, cnpj_condominio) VALUES(?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, ocorrencia.getTitulo());
		statement.setString(2, ocorrencia.getDescricao());
		statement.setDate(3, java.sql.Date.valueOf(ocorrencia.getData_ocorrencia()));
		statement.setString(4, ocorrencia.getnome_usuario());
		statement.setString(5, ocorrencia.getCnpj_condominio());

		statement.execute();

	}

	public List<ComunicadosVO> getComunicados(String cnpj) throws Exception {
		List<ComunicadosVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_comunicados WHERE cnpj_condominio=?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cnpj);
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			ComunicadosVO comunicados = new ComunicadosVO();
			comunicados.setId_ocorrencia(rs.getInt("id_ocorrencia"));
			comunicados.setTitulo(rs.getString("titulo"));
			comunicados.setDescricao(rs.getString("descricao"));
			comunicados.setData_ocorrencia(rs.getString("data_ocorrencia"));
			comunicados.setNome_usuario(rs.getString("nome_usuario"));
			comunicados.setCnpj_condominio(rs.getString("cnpj_condominio"));
			lista.add(comunicados);
		}

		return lista;

	}

	public void deleteComunicado(int id) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "DELETE FROM tb_comunicados WHERE id_ocorrencia = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setInt(1, id);
		statement.execute();

	}
}
