package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.AreaComum;
import br.com.condoservices.entidade.AreaComumVO;


public class AreaComumDAO {
	public void addArea(AreaComum area) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_area_comum (nome, descricao, valor, detalhes, limite_horario, limite_pessoas, cnpj_condominio, tipo_espaco) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, area.getNome());
		statement.setString(2, area.getDescricao());
		statement.setString(3, area.getValor());
		statement.setString(4, area.getDetalhes());
		statement.setTime(5, java.sql.Time.valueOf(area.getLimite_horario()));
		statement.setInt(6, area.getLimite_pessoas());
		statement.setString(7, area.getCnpj_condominio());
		statement.setString(8, area.getTipo_espaco());

		statement.execute();

	}

	public List<AreaComumVO> getAreas(String cnpj) throws Exception {
		List<AreaComumVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_area_comum WHERE cnpj_condominio=?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cnpj);
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			AreaComumVO area = new AreaComumVO();
			area.setId_area(rs.getInt("id_area"));
			area.setNome(rs.getString("nome"));
			area.setDescricao(rs.getString("descricao"));
			area.setValor(rs.getDouble("valor"));
			area.setDetalhes(rs.getString("detalhes"));
			area.setLimite_horario(rs.getString("limite_horario"));
			area.setLimite_pessoas(rs.getInt("limite_pessoas"));
			area.setCnpj_condominio(rs.getString("cnpj_condominio"));
			area.setTipo_espaco(rs.getString("tipo_espaco"));
			lista.add(area);
		}

		return lista;

	}

	public void deleteAreaComum(int id) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "DELETE FROM tb_area_comum WHERE id_area = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setInt(1, id);
		statement.execute();

	}

	public void deleteReserva(int id) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "DELETE FROM tb_reservas WHERE id_reserva = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setInt(1, id);
		statement.execute();

	}

}
