package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;

import br.com.condoservices.entidade.Login;
import br.com.condoservices.entidade.TipoUsuario;
import br.com.condoservices.entidade.Usuarios;

public class UsuarioDAO {

	public List<Login> getLogin(String login, String senha) throws Exception {
		List<Login> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT login, descricao AS tipo, cpf, nome, cnpj, razao_social, tb_apartamentos.numero, bloco, andar, primeiro_acesso FROM tb_usuarios \r\n"
				+ "INNER JOIN tb_pessoas ON tb_usuarios.cpf_pessoa = tb_pessoas.cpf\r\n"
				+ "INNER JOIN tb_condominio ON tb_pessoas.cnpj_condominio = tb_condominio.cnpj\r\n"
				+ "INNER JOIN tb_apartamentos ON tb_pessoas.apto = tb_apartamentos.id_apto\r\n"
				+ "INNER JOIN tb_tipo_usuario ON tb_tipo_usuario.id_tipo = tb_usuarios.tipo\r\n"
				+ "WHERE login=? AND senha=? AND status_usuario = 'A'";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, login);
		statement.setString(2, senha);

		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			Login rsLogin = new Login();
			rsLogin.setLogin(rs.getString("login"));
			rsLogin.setTipo(rs.getString("tipo"));
			rsLogin.setCpf(rs.getString("cpf"));
			rsLogin.setNome(rs.getString("nome"));
			rsLogin.setCnpj(rs.getString("cnpj"));
			rsLogin.setRazao_social(rs.getString("razao_social"));
			rsLogin.setNumero(rs.getString("numero"));
			rsLogin.setBloco(rs.getString("bloco"));
			rsLogin.setAndar(rs.getString("andar"));
			rsLogin.setPrimeiro_acesso(rs.getBoolean("primeiro_acesso"));
			rsLogin.setAuth(true);
			lista.add(rsLogin);
		}

		return lista;

	}

	public String addUser(Usuarios usuario) throws Exception {
		String msgRetorno = null;
		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_usuarios (login, senha, status_usuario, tipo, data_cadastro, data_alteracao, cpf_pessoa, primeiro_acesso ) VALUES (?,?,?,?,?,?,?,?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, usuario.getLogin());
		statement.setString(2, usuario.getSenha());
		statement.setString(3, usuario.getStatus_usuario());
		statement.setInt(4, usuario.getTipo());
		statement.setDate(5, java.sql.Date.valueOf(usuario.getData_cadastro()));
		statement.setDate(6, java.sql.Date.valueOf(usuario.getData_alteracao()));
		statement.setString(7, usuario.getCpf_pessoa());
		statement.setBoolean(8, true);

		statement.execute();

		return msgRetorno;
	}

	public List<TipoUsuario> getListTipoUsuario() throws Exception {
		Connection conexao = DBConfig.getConnection();
		List<TipoUsuario> lista = new ArrayList<>();

		String sql = "SELECT * FROM tb_tipo_usuario";

		PreparedStatement statement = conexao.prepareStatement(sql);

		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			TipoUsuario tipo = new TipoUsuario();
			tipo.setId_tipo(rs.getInt("id_tipo"));
			tipo.setDescricao(rs.getString("descricao"));
			lista.add(tipo);
		}

		return lista;
	}

	public void updatePrimeiroAcesso(String login, String senha) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "UPDATE db_condoservices.tb_usuarios SET primeiro_acesso=0, senha=? WHERE login=?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, senha);
		statement.setString(2, login);

		statement.execute();

	}

}
