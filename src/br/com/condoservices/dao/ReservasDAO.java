package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.Reservas;
import br.com.condoservices.entidade.ReservasVO;

public class ReservasDAO {

	public boolean addReserva(Reservas reserva) throws Exception {
		Connection conexao = DBConfig.getConnection();
		
		
		if(verificaReserva(reserva.getCnpj_condominio(), reserva.getData_ini_reserva(), reserva.getId_area(), reserva.getHorario_ini())) {
			String sql = "INSERT INTO tb_reservas (data_ini_reserva, data_fim_reserva, id_area, login_usuario, cnpj_condominio, horario_ini, horario_fim) VALUES (?,?,?,?,?,?,?)";

			PreparedStatement statement = conexao.prepareStatement(sql);
			statement.setDate(1, java.sql.Date.valueOf(reserva.getData_ini_reserva()));
			statement.setDate(2, java.sql.Date.valueOf(reserva.getData_fim_reserva()));
			statement.setInt(3, reserva.getId_area());
			statement.setString(4, reserva.getLogin_usuario());
			statement.setString(5, reserva.getCnpj_condominio());
			if(reserva.getHorario_ini() == "" || reserva.getHorario_ini() == null) {
				statement.setTime(6, null);
				statement.setTime(7, null);
			}else {
				statement.setTime(6, java.sql.Time.valueOf(reserva.getHorario_ini()));
				statement.setTime(7, java.sql.Time.valueOf(reserva.getHorario_fim()));
			}
			
			
			statement.execute();
						
			return true;
		}else {
			
			return false;
		}
			
	}

	public List<ReservasVO> getReservas(String cnpj) throws Exception {
		List<ReservasVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT nome, data_ini_reserva, data_fim_reserva\r\n" 
				+ "FROM tb_reservas \r\n"
				+ "INNER JOIN tb_area_comum ON tb_reservas.id_area = tb_area_comum.id_area\r\n"
				+ "WHERE tb_reservas.cnpj_condominio=? AND data_ini_reserva >= CURDATE() ORDER BY data_ini_reserva ASC";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cnpj);

		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			ReservasVO reserva = new ReservasVO();
			reserva.setNome(rs.getString("nome"));
			reserva.setData_ini_reserva(rs.getString("data_ini_reserva"));
			reserva.setData_fim_reserva(rs.getString("data_fim_reserva"));
			lista.add(reserva);
		}

		return lista;

	}

	private boolean verificaReserva(String cnpj, String data, int id, String horario) throws Exception {

		Connection conexao = DBConfig.getConnection();
		
		String sql;
		
		if(horario != null) {
			sql = "SELECT * FROM tb_reservas WHERE cnpj_condominio=? AND data_ini_reserva=? AND id_area=? AND horario_ini=?";
		}else {
			sql = "SELECT * FROM tb_reservas WHERE cnpj_condominio=? AND data_ini_reserva=? AND id_area=?";
		}

		
		
		

		PreparedStatement statement = conexao.prepareStatement(sql);
		
		if(horario != null) {
			statement.setString(1, cnpj);
			statement.setDate(2, java.sql.Date.valueOf(data));
			statement.setInt(3, id);
			statement.setTime(4, java.sql.Time.valueOf(horario));
		}else {
			statement.setString(1, cnpj);
			statement.setDate(2, java.sql.Date.valueOf(data));
			statement.setInt(3, id);
		}
		
		
		System.out.println(sql);

		

		ResultSet rs = statement.executeQuery();

		if (rs.next()) {
			return false;
		} else {
			return true;
		}

	}

}
