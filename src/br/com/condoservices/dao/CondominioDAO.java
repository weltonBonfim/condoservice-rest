package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import br.com.condoservices.entidade.Condominio;
import br.com.condoservices.config.DBConfig;

public class CondominioDAO {
	public List<Condominio> listarCondominio() throws Exception {
		List<Condominio> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_condominio";

		PreparedStatement statement = conexao.prepareStatement(sql);
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			Condominio condominio = new Condominio();
			condominio.setCnpj(rs.getString("cnpj"));
			condominio.setRazao_social(rs.getString("razao_social"));
			condominio.setInscricao_estadual(rs.getString("inscricao_estadual"));
			condominio.setSituacao(rs.getString("situacao"));
			condominio.setEndereco(rs.getString("endereco"));
			condominio.setNumero(rs.getInt("numero"));
			condominio.setBairro(rs.getString("bairro"));
			condominio.setCidade(rs.getString("cidade"));
			condominio.setEstado(rs.getString("estado"));
			condominio.setCep(rs.getString("cep"));
			condominio.setTelefone(rs.getString("telefone"));
			condominio.setEmail(rs.getString("email"));
			condominio.setData_cadastro(String.valueOf(rs.getDate("data_cadastro")));
			lista.add(condominio);
		}

		return lista;
	}

	public String addCondominio(Condominio condominio) throws Exception {
		String msgRetorno = null;
		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_condominio (cnpj, razao_social, inscricao_estadual, situacao, endereco, numero, bairro, cidade, estado, cep, telefone, email, data_cadastro) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, condominio.getCnpj());
		statement.setString(2, condominio.getRazao_social());
		statement.setString(3, condominio.getInscricao_estadual());
		statement.setString(4, condominio.getSituacao());
		statement.setString(5, condominio.getEndereco());
		statement.setInt(6, condominio.getNumero());
		statement.setString(7, condominio.getBairro());
		statement.setString(8, condominio.getCidade());
		statement.setString(9, condominio.getEstado());
		statement.setString(10, condominio.getCep());
		statement.setString(11, condominio.getTelefone());
		statement.setString(12, condominio.getEmail());
		statement.setDate(13, java.sql.Date.valueOf(condominio.getData_cadastro()));

		statement.execute();

		return msgRetorno;
	}

	public void removerCondominio(int idCondominio) throws Exception {
		Connection conexao = DBConfig.getConnection();

		String sql = "DELETE FROM tb_condominio WHERE cnpj = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setInt(1, idCondominio);
		statement.execute();
	}

}
