package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.Ocorrencias;
import br.com.condoservices.entidade.OcorrenciasVO;

public class OcorrenciasDAO {
	public void addOcorrencia(Ocorrencias ocorrencia) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_ocorrencias (titulo, mensagem_in, mensagem_out, status_ocorrencia, data_ocorrencia, login_usuario, cnpj_condominio) VALUES(?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, ocorrencia.getTitulo());
		statement.setString(2, ocorrencia.getMensagem_in());
		statement.setString(3, ocorrencia.getMensagem_out());
		statement.setString(4, ocorrencia.getStatus_ocorrencia());
		statement.setDate(5, java.sql.Date.valueOf(ocorrencia.getData_ocorrencia()));
		statement.setString(6, ocorrencia.getLogin_usuario());
		statement.setString(7, ocorrencia.getCnpj_condominio());

		statement.execute();

	}

	public List<OcorrenciasVO> getOcorrencia(String cnpj, String tipo, String login, String status) throws Exception {
		List<OcorrenciasVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql;

		if (tipo.equals("Condomino")) {
			sql = "SELECT id_ocorrencia, titulo, mensagem_in, mensagem_out, status_ocorrencia, data_ocorrencia, login_usuario, cnpj_condominio FROM tb_ocorrencias oco INNER JOIN tb_usuarios usu ON oco.login_usuario = usu.login  WHERE cnpj_condominio=? AND login_usuario=? AND status_ocorrencia=?";
		} else {
			sql = "SELECT * FROM tb_ocorrencias WHERE cnpj_condominio=? AND status_ocorrencia=?";
		}

		PreparedStatement statement = conexao.prepareStatement(sql);

		if (tipo.equals("Condomino")) {
			statement.setString(1, cnpj);
			statement.setString(2, login);
			statement.setString(3, status);
		} else {
			statement.setString(1, cnpj);
			statement.setString(2, status);
		}

		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			OcorrenciasVO ocorrencias = new OcorrenciasVO();
			ocorrencias.setId_ocorrencia(rs.getInt("id_ocorrencia"));
			ocorrencias.setTitulo(rs.getString("titulo"));
			ocorrencias.setMensagem_in(rs.getString("mensagem_in"));
			ocorrencias.setMensagem_out(rs.getString("mensagem_out"));
			ocorrencias.setStatus_ocorrencia(rs.getString("status_ocorrencia"));
			ocorrencias.setData_ocorrencia(rs.getString("data_ocorrencia"));
			ocorrencias.setLogin_usuario(rs.getString("login_usuario"));
			ocorrencias.setCnpj_condominio(rs.getString("cnpj_condominio"));
			lista.add(ocorrencias);
		}

		return lista;

	}

	public void encerrarOcorrencia(int id, String mensagem_out, String status) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "UPDATE tb_ocorrencias SET mensagem_out=?, status_ocorrencia=? WHERE id_ocorrencia = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, mensagem_out);
		statement.setString(2, status);
		statement.setInt(3, id);
		statement.execute();

	}

	public void reabrirOcorrencia(int id, String status) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "UPDATE tb_ocorrencias SET status_ocorrencia=? WHERE id_ocorrencia = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, status);
		statement.setInt(2, id);
		statement.execute();

	}

}
