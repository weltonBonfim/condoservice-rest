package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.Apartamentos;
import br.com.condoservices.entidade.ApartamentosVO;

public class AptoDAO {
	public void addApto(Apartamentos apto) throws Exception {
		
		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_apartamentos (numero, bloco, andar, cnpj_condominio) VALUES(?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, apto.getNumero());
		statement.setString(2, apto.getBloco());
		statement.setString(3, apto.getAndar());
		statement.setString(4, apto.getCnpj_condominio());

		statement.execute();

	}
	
	public List<ApartamentosVO> listarApto(String cnpj_condominio) throws Exception {
		List<ApartamentosVO> lista = new ArrayList<>();

		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT id_apto, numero, bloco, andar FROM tb_apartamentos WHERE cnpj_condominio = ?";
		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cnpj_condominio);
			
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			ApartamentosVO aptoVO = new ApartamentosVO();
			aptoVO.setId(rs.getInt("id_apto"));
			aptoVO.setNumero(rs.getString("numero"));
			aptoVO.setBloco(rs.getString("bloco"));
			aptoVO.setAndar(rs.getString("andar"));
					
			lista.add(aptoVO);
		}

		return lista;
	}

}
