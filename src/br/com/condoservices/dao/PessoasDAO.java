package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.Pessoas;
import br.com.condoservices.entidade.PessoasVO;


public class PessoasDAO {

	public String addPessoas(Pessoas pessoas) throws Exception {
		String msgRetorno = null;
		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_pessoas (cpf, rg, nome, endereco, numero, bairro, cidade, estado, cep, telefone, email, data_nascimento, data_cadastro, cnpj_condominio, apto) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, pessoas.getCpf());
		statement.setString(2, pessoas.getRg());
		statement.setString(3, pessoas.getNome());
		statement.setString(4, pessoas.getEndereco());
		statement.setInt(5, pessoas.getNumero());
		statement.setString(6, pessoas.getBairro());
		statement.setString(7, pessoas.getCidade());
		statement.setString(8, pessoas.getEstado());
		statement.setString(9, pessoas.getCep());
		statement.setString(10, pessoas.getTelefone());
		statement.setString(11, pessoas.getEmail());
		statement.setDate(12, java.sql.Date.valueOf(pessoas.getData_nascimento()));
		statement.setDate(13, java.sql.Date.valueOf(pessoas.getData_cadastro()));
		statement.setString(14, pessoas.getCnpj_condominio());
		statement.setInt(15, pessoas.getId_apto());

		statement.execute();

		return msgRetorno;
	}

	public List<PessoasVO> getListPessoas() throws Exception {
		Connection conexao = DBConfig.getConnection();
		List<PessoasVO> lista = new ArrayList<>();

		String sql = "SELECT cpf, nome FROM tb_pessoas";

		PreparedStatement statement = conexao.prepareStatement(sql);

		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			PessoasVO pessoas = new PessoasVO();
			pessoas.setCpf(rs.getString("cpf"));
			pessoas.setNome(rs.getString("nome"));
			lista.add(pessoas);
		}

		return lista;
	}
}
