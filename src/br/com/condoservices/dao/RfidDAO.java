package br.com.condoservices.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.condoservices.config.DBConfig;
import br.com.condoservices.entidade.ReservasVO;
import br.com.condoservices.entidade.Rfid;

import java.util.ArrayList;
import java.util.List;



public class RfidDAO {

	public String validEntrada(String rfid) throws Exception {
		
		String retorno = ""; 
		
		if (verificaRFIDCadastrado(rfid).equals("ACESSO LIBERADO")) {
			retorno = "ACESSO LIBERADO";
		} else if (verificaRFIDCadastrado(rfid).equals("ACESSO NEGADO")){
			retorno = "ACESSO NEGADO";
		} else if (verificaRFIDCadastrado(rfid).equals("NAO CADASTRADO")) {
			insertRFID(rfid);
			retorno = "RFID NAO CADASTRADA";
		}
		
		return retorno;

	}

	public String verificaRFIDCadastrado(String rfid) throws Exception {
		Connection conexao = DBConfig.getConnection();

		String sql = "SELECT status FROM tb_rfid WHERE rfid = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, rfid);

		ResultSet rs = statement.executeQuery();

		if (rs.next()) {
			if (rs.getBoolean("status")) {
				return "ACESSO LIBERADO";
			} else {
				return "ACESSO NEGADO";
			}
		} else {
			return "NAO CADASTRADO";
		}

	}

	public void insertRFID(String rfid) throws Exception {
		Connection conexao = DBConfig.getConnection();

		String sql = "INSERT INTO tb_rfid (rfid, status) VALUES (?,?)";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, rfid);
		statement.setBoolean(2, false);

		statement.execute();

	}

	public void updateStatusRFID(String id) throws Exception {

		Connection conexao = DBConfig.getConnection();

		String sql = "UPDATE db_condoservices.tb_rfid SET status = 1 WHERE id_rfid=?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, id);

		statement.execute();

	}
	
	public List<Rfid> listaRFID() throws Exception {
		Connection conexao = DBConfig.getConnection();
		
		List<Rfid> lista = new ArrayList<>();
		
		
		String sql = "SELECT * FROM tb_rfid WHERE status = 0";

		PreparedStatement statement = conexao.prepareStatement(sql);
		
		ResultSet rs = statement.executeQuery();

		while (rs.next()) {
			Rfid rfid = new Rfid();
			rfid.setId_rfid(rs.getInt("id_rfid"));
			rfid.setRfid(rs.getString("rfid"));
			rfid.setStatus(rs.getBoolean("status"));
			lista.add(rfid);
		}

		return lista;
		
	}

}
