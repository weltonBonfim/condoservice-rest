package br.com.condoservices.entidade;

public class Reservas {
	String data_ini_reserva;
	String data_fim_reserva;
	int id_area;
	String login_usuario;
	String cnpj_condominio;
	String horario_ini;
	String horario_fim;
	
	public String getHorario_ini() {
		return horario_ini;
	}

	public void setHorario_ini(String horario_ini) {
		this.horario_ini = horario_ini;
	}

	public String getHorario_fim() {
		return horario_fim;
	}

	public void setHorario_fim(String horario_fim) {
		this.horario_fim = horario_fim;
	}

	public String getData_ini_reserva() {
		return data_ini_reserva;
	}

	public void setData_ini_reserva(String data_ini_reserva) {
		this.data_ini_reserva = data_ini_reserva;
	}

	public String getData_fim_reserva() {
		return data_fim_reserva;
	}

	public void setData_fim_reserva(String data_fim_reserva) {
		this.data_fim_reserva = data_fim_reserva;
	}

	public int getId_area() {
		return id_area;
	}

	public void setId_area(int id_area) {
		this.id_area = id_area;
	}

	public String getLogin_usuario() {
		return login_usuario;
	}

	public void setLogin_usuario(String login_usuario) {
		this.login_usuario = login_usuario;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}

}
