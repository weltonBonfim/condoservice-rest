package br.com.condoservices.entidade;

public class Rfid {
	int id_rfid;
	String rfid;
	boolean status;

	public int getId_rfid() {
		return id_rfid;
	}

	public void setId_rfid(int id_rfid) {
		this.id_rfid = id_rfid;
	}

	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
