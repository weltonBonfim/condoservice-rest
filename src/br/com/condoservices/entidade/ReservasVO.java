package br.com.condoservices.entidade;

public class ReservasVO {
	String nome;
	String data_ini_reserva;
	String data_fim_reserva;
	String horario_ini;
	String horario_fim;
	
	public String getHorario_ini() {
		return horario_ini;
	}

	public void setHorario_ini(String horario_ini) {
		this.horario_ini = horario_ini;
	}

	public String getHorario_fim() {
		return horario_fim;
	}

	public void setHorario_fim(String horario_fim) {
		this.horario_fim = horario_fim;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getData_ini_reserva() {
		return data_ini_reserva;
	}

	public void setData_ini_reserva(String data_ini_reserva) {
		this.data_ini_reserva = data_ini_reserva;
	}

	public String getData_fim_reserva() {
		return data_fim_reserva;
	}

	public void setData_fim_reserva(String data_fim_reserva) {
		this.data_fim_reserva = data_fim_reserva;
	}

}
