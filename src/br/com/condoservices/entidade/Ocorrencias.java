package br.com.condoservices.entidade;

public class Ocorrencias {
	String titulo;
	String mensagem_in;
	String mensagem_out;
	String status_ocorrencia;
	String data_ocorrencia;
	String login_usuario;
	String cnpj_condominio;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem_in() {
		return mensagem_in;
	}

	public void setMensagem_in(String mensagem_in) {
		this.mensagem_in = mensagem_in;
	}

	public String getMensagem_out() {
		return mensagem_out;
	}

	public void setMensagem_out(String mensagem_out) {
		this.mensagem_out = mensagem_out;
	}

	public String getStatus_ocorrencia() {
		return status_ocorrencia;
	}

	public void setStatus_ocorrencia(String status_ocorrencia) {
		this.status_ocorrencia = status_ocorrencia;
	}

	public String getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(String data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getLogin_usuario() {
		return login_usuario;
	}

	public void setLogin_usuario(String login_usuario) {
		this.login_usuario = login_usuario;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}

}
