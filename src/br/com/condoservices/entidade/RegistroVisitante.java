package br.com.condoservices.entidade;

public class RegistroVisitante {
	String nome_visitante;
	String motivo_visita;
	String data_entrada;
	int id_apto;
	String cnpj_condominio;
	String doc_visitante;
	String data_saida;

	public String getData_saida() {
		return data_saida;
	}

	public void setData_saida(String data_saida) {
		this.data_saida = data_saida;
	}

	public String getDoc_visitante() {
		return doc_visitante;
	}

	public void setDoc_visitante(String doc_visitante) {
		this.doc_visitante = doc_visitante;
	}

	public String getNome_visitante() {
		return nome_visitante;
	}

	public void setNome_visitante(String nome_visitante) {
		this.nome_visitante = nome_visitante;
	}

	public String getMotivo_visita() {
		return motivo_visita;
	}

	public void setMotivo_visita(String motivo_visita) {
		this.motivo_visita = motivo_visita;
	}

	public String getdata_entrada() {
		return data_entrada;
	}

	public void setdata_entrada(String data_entrada) {
		this.data_entrada = data_entrada;
	}

	public int getId_apto() {
		return id_apto;
	}

	public void setId_apto(int id_apto) {
		this.id_apto = id_apto;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}

}
