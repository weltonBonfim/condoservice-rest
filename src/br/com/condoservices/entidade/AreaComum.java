package br.com.condoservices.entidade;

public class AreaComum {
	String nome;
	String descricao;
	String valor;
	String detalhes;
	String limite_horario;
	int limite_pessoas;
	String cnpj_condominio;
	String tipo_espaco;
	
	
	public String getTipo_espaco() {
		return tipo_espaco;
	}

	public void setTipo_espaco(String tipo_espaco) {
		this.tipo_espaco = tipo_espaco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}

	public String getLimite_horario() {
		return limite_horario;
	}

	public void setLimite_horario(String limite_horario) {
		this.limite_horario = limite_horario;
	}

	public int getLimite_pessoas() {
		return limite_pessoas;
	}

	public void setLimite_pessoas(int limite_pessoas) {
		this.limite_pessoas = limite_pessoas;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}

}
