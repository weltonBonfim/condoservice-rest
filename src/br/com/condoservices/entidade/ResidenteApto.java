package br.com.condoservices.entidade;

public class ResidenteApto {
	int id_residente;
	String tipo_residente;
	int id_apto;
	char cpf_residente;

	public int getId_residente() {
		return id_residente;
	}

	public void setId_residente(int id_residente) {
		this.id_residente = id_residente;
	}

	public String getTipo_residente() {
		return tipo_residente;
	}

	public void setTipo_residente(String tipo_residente) {
		this.tipo_residente = tipo_residente;
	}

	public int getId_apto() {
		return id_apto;
	}

	public void setId_apto(int id_apto) {
		this.id_apto = id_apto;
	}

	public char getCpf_residente() {
		return cpf_residente;
	}

	public void setCpf_residente(char cpf_residente) {
		this.cpf_residente = cpf_residente;
	}

}
