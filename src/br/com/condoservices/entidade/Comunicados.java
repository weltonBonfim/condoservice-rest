package br.com.condoservices.entidade;

public class Comunicados {
	String titulo;
	String descricao;
	String data_ocorrencia;
	String nome_usuario;
	String cnpj_condominio;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(String data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getnome_usuario() {
		return nome_usuario;
	}

	public void setnome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}
}
