package br.com.condoservices.entidade;

import java.util.Date;

public class Dependentes {
	char cpf_dependente;
	String nome;
	String tipo_dependente;
	Date data_nascimento;
	char cpf_residente;

	public char getCpf_dependente() {
		return cpf_dependente;
	}

	public void setCpf_dependente(char cpf_dependente) {
		this.cpf_dependente = cpf_dependente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo_dependente() {
		return tipo_dependente;
	}

	public void setTipo_dependente(String tipo_dependente) {
		this.tipo_dependente = tipo_dependente;
	}

	public Date getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public char getCpf_residente() {
		return cpf_residente;
	}

	public void setCpf_residente(char cpf_residente) {
		this.cpf_residente = cpf_residente;
	}

}
