package br.com.condoservices.entidade;

public class Apartamentos {
	String numero;
	String bloco;
	String andar;
	String cnpj_condominio;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBloco() {
		return bloco;
	}

	public void setBloco(String bloco) {
		this.bloco = bloco;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public String getCnpj_condominio() {
		return cnpj_condominio;
	}

	public void setCnpj_condominio(String cnpj_condominio) {
		this.cnpj_condominio = cnpj_condominio;
	}

}
