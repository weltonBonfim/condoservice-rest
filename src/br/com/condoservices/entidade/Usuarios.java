package br.com.condoservices.entidade;

public class Usuarios {
	String login;
	String senha;
	String status_usuario;
	int tipo;
	String data_cadastro;
	String data_alteracao;
	String cpf_pessoa;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getStatus_usuario() {
		return status_usuario;
	}

	public void setStatus_usuario(String status_usuario) {
		this.status_usuario = status_usuario;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(String data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public String getData_alteracao() {
		return data_alteracao;
	}

	public void setData_alteracao(String data_alteracao) {
		this.data_alteracao = data_alteracao;
	}

	public String getCpf_pessoa() {
		return cpf_pessoa;
	}

	public void setCpf_pessoa(String cpf_pessoa) {
		this.cpf_pessoa = cpf_pessoa;
	}

}
