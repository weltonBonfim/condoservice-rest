package br.com.condoservices.entidade;

public class Veiculos {
	int id_carro;
	String modelo;
	String marca;
	String placa;
	String cor;
	char cpf_residente;

	public int getId_carro() {
		return id_carro;
	}

	public void setId_carro(int id_carro) {
		this.id_carro = id_carro;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public char getCpf_residente() {
		return cpf_residente;
	}

	public void setCpf_residente(char cpf_residente) {
		this.cpf_residente = cpf_residente;
	}

}
